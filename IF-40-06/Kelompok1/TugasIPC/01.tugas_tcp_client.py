# import library socket karena akan menggunakan IPC socket
import socket

# definisikan tujuan IP server
TCP_IP = '192.168.43.2'

# definisikan port dari server yang akan terhubung
TCP_PORT = 12345

# definisikan ukuran buffer untuk mengirimkan pesan
BUFFER_SIZE = 1024

# definisikan pesan yang akan disampaikan
MESSAGE = "Hello my name is Dean"

# buat socket TCP
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

# lakukan koneksi ke server dengan parameter IP dan Port yang telah didefinisikan
s.connect((TCP_IP, TCP_PORT))

# kirim pesan ke server
#s.send(MESSAGE)
s.send(MESSAGE.encode())

# terima pesan dari server
data = s.recv(BUFFER_SIZE).decode()

# tampilkan pesan/reply dari server
print ("Data diterima", data)

# tutup koneksi
s.close()
