# import library socket karena akan menggunakan IPC socket
import socket

# definisikan alamat IP bind dari server
UDP_IP = "192.168.43.2"

# definisikan port number untuk bind dari server
UDP_PORT = 12345

# buat socket bertipe UDP
udp_sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)

# lakukan bind
udp_sock.bind((UDP_IP, UDP_PORT))
print("Binding...")


# loop forever
while True:
    # terima pesan dari client
    data, addr = udp_sock.recvfrom(1024)
    
    # menampilkan hasil pesan dari client
    print("Received message : ", data.decode())
    
    
