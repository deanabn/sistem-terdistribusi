# import library socket karena akan menggunakan IPC socket
import socket

# definisikan alamat IP binding  yang akan digunakan 
TCP_IP = '192.168.43.2'

# definisikan port number binding  yang akan digunakan 
TCP_PORT = 12345

# definisikan ukuran buffer untuk mengirimkan pesan
BUFFER_SIZE = 1024

# buat socket bertipe TCP
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

# lakukan bind
s.bind((TCP_IP, TCP_PORT))
print("Binding...")

# server akan listen menunggu hingga ada koneksi dari client
s.listen(2)
print("Listening...")

# lakukan loop forever
while 2:
	# menerima koneksi
	conn, addr = s.accept()
	print("Receiving Connection...")
	
	# menampilkan koneksi berupa IP dan port client yang terhubung menggunakan print
	print("Connection address : ", addr)
	
	# menerima data berdasarkan ukuran buffer
	data = conn.recv(BUFFER_SIZE).decode()
	
	# menampilkan pesan yang diterima oleh server menggunakan print
	if not data: break
	print("Received data : ", data)
	
	# mengirim kembali data yang diterima dari client kepada client
	conn.send(data.encode())

# tutup koneksi	
conn.close()
