# import library socket karena akan menggunakan IPC socket
import socket

# definisikan target IP server yang akan dituju
UDP_IP = "192.168.43.2"

# definisikan target port number server yang akan dituju
UDP_PORT = 12345

PESAN = "Nama Saya Dean".encode()

print ("target IP:", UDP_IP)
print ("target port:", UDP_PORT)
#print ("pesan:", PESAN)

# buat socket bertipe UDP
sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)

# lakukan loop 10 kali
for x in range (10):
    # definisikan pesan yang akan dikirim
    #sock.sendto(MESSAGE, (UDP_IP, UDP_PORT))
    PESAN = "Nama Saya Dean"
    PESAN.encode()
    # kirim pesan    
    sock.sendto(PESAN, (UDP_IP, UDP_PORT))

print(x," Pesan berhasil dikirim")