# import library socket karena menggunakan IPC socket
import socket

# definisikan IP server tujuan file akan diupload
TCP_IP = '192.168.0.145'

# definisikan port number proses di server
TCP_PORT = 12345

# definisikan ukuran buffer untuk mengirim
BUFFER_SIZE = 1024

# buat socket (apakah bertipe UDP atau TCP?)
s = socket.socket() 

# lakukan koneksi ke server
s.connect((TCP_IP, TCP_PORT))

# buka file bernama "hasil_download.txt bertipe byte
# masih hard code, file harus ada dalam folder yang sama dengan script python
file = open('hasil_download.txt', 'wb')
	#print ('file opened')

# loop forever
while 1:
    # terima pesan dari client
    data = s.recv(BUFFER_SIZE)
    
    # tulis pesan yang diterima dari client ke file kita (result.txt)
    file.write(data)
    
    # berhenti jika sudah tidak ada pesan yang dikirim
    if not data: break
    
# tutup file_hasil_download.txt    
file.close()
print('Successfully get the file')

#tutup socket
s.close()
print('connection closed')