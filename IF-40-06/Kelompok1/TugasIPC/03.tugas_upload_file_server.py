# import library socket karena menggunakan IPC socket
import socket

# definisikan IP untuk binding
TCP_IP = '192.168.0.145'

# definisikan port untuk binding
TCP_PORT = 12345

# definisikan ukuran buffer untuk menerima pesan
BUFFER_SIZE = 1024

# buat socket (bertipe UDP atau TCP?)
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

# lakukan binding ke IP dan port
s.bind((TCP_IP, TCP_PORT))
print("Binding...")

# lakukan listen
s.listen(2)
print("Listening...")

# loop forever
while 1:
	# Siap menerima koneksi
	conn, addr = s.accept()
	print ('Connection address:', addr)

	# Buka/buat file bernama hasil_upload.txt untuk menyimpan hasil dari file yang dikirim server
	f = open('hasil_upload.txt','wb')
	while 1:
		# terima pesan dari client
		data = conn.recv(1024)

		if data:
			f.write(data)
		else:
			f.close()
			print("end receiving")
			break
	# berhenti jika sudah tidak ada pesan yang dikirim
	if not data: break

#tutup socket
s.close()

# tutup koneksi
conn.close()
